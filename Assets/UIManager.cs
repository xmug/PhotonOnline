using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject startGamePanel;
    public GameObject ConnectingPanel;
    public GameObject EnterRoomPanel;
    
    public Button startGameBtn;
    public InputField nameInputField;
    public Text welcomeText;

    private void Awake()
    {
        startGameBtn.onClick.AddListener(() =>
        {
            // 表现层
            startGamePanel.SetActive(false);
            ConnectingPanel.SetActive(true);
            
            // 逻辑层
            GameModel.name = nameInputField.text;
            StartGameEvent.Trigger();
        });
        
        connectionSuccessEvent.Register(() =>
        {
            ConnectingPanel.SetActive(false);
            EnterRoomPanel.SetActive(true);
                welcomeText.text = "Welcome! " + PhotonNetwork.NickName;
        });
    }
}

public class connectionSuccessEvent : Event<connectionSuccessEvent>
{
    
}

public class StartGameEvent : Event<StartGameEvent>
{
    
}

public class Event<T> where T : Event<T>
{
    private static Action _event;

    public static void Register(Action mEvent)
    {
        _event += mEvent;
    }
    
    public static void UnRegister(Action mEvent)
    {
        _event -= mEvent;
    }
    
    public static void Trigger()
    {
        _event.Invoke();
    }
}
