using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using Photon.Pun;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    private void Awake()
    {
        StartGameEvent.Register((() =>
        {
            PhotonNetwork.NickName = GameModel.name;
            PhotonNetwork.ConnectUsingSettings();
        }));
    }

    public override void OnConnectedToMaster()
    {
        connectionSuccessEvent.Trigger();
        Debug.Log("Welcome! " + PhotonNetwork.NickName);
    }
}